package com.shepherdmoney.interviewproject.controller;

import com.shepherdmoney.interviewproject.model.CreditCard;
import com.shepherdmoney.interviewproject.model.User;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import com.shepherdmoney.interviewproject.vo.request.CreateUserPayload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.NoSuchElementException;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @PutMapping("/user")
    public ResponseEntity<Integer> createUser(@RequestBody CreateUserPayload payload) {
        String userName = payload.getName();
        String email = payload.getEmail();

        // Validate that the username and email are not empty/whitespace.
        if (userName.isBlank() || email.isBlank()) {
            return ResponseEntity.badRequest().body(400);
        }

        // Initialize user object from payload and save
        User user = new User();
        user.setName(payload.getName());
        user.setEmail(payload.getEmail());
        user.setCards(new ArrayList<>());
        userRepository.save(user);
        return ResponseEntity.ok(user.getId());
    }

    @DeleteMapping("/user")
    public ResponseEntity<String> deleteUser(@RequestParam int userId) {
        // Validate userID. Returns 400 bad request if id is null, not found in repository, or if user obj failed to return.
        User user;
        try {
            user = userRepository.findById(userId).orElseThrow();
        } catch (IllegalArgumentException | EmptyResultDataAccessException | NoSuchElementException ex) {
            return ResponseEntity.badRequest().body(String.format("Deletion unsuccessful, user with ID: %d does not exist.", userId)); // 400 Bad Request
        }

        // Remove this user from all of their cards
        for (CreditCard card: user.getCards()) {
            card.setUser(null);
        }

        userRepository.delete(user);
        return ResponseEntity.ok(String.format("Successfully deleted user with ID: %d.", userId));
    }
}
