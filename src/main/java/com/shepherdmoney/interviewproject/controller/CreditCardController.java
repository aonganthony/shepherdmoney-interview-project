package com.shepherdmoney.interviewproject.controller;

import com.shepherdmoney.interviewproject.model.BalanceHistory;
import com.shepherdmoney.interviewproject.model.User;
import com.shepherdmoney.interviewproject.model.CreditCard;
import com.shepherdmoney.interviewproject.repository.UserRepository;
import com.shepherdmoney.interviewproject.repository.CreditCardRepository;
import com.shepherdmoney.interviewproject.vo.request.AddCreditCardToUserPayload;
import com.shepherdmoney.interviewproject.vo.request.UpdateBalancePayload;
import com.shepherdmoney.interviewproject.vo.response.CreditCardView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class CreditCardController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CreditCardRepository creditCardRepository;


    @PostMapping("/credit-card")
    public ResponseEntity<Integer> addCreditCardToUser(@RequestBody AddCreditCardToUserPayload payload) {
        // Validate userID. Returns 400 bad request if id is null, not found in repository, or if user failed to return.
        User user;
        try {
            user = userRepository.findById(payload.getUserId()).orElseThrow();
        } catch (IllegalArgumentException | EmptyResultDataAccessException | NoSuchElementException ex) {
            return ResponseEntity.badRequest().body(400); // 400 Bad Request
        }

        String cardNumber = payload.getCardNumber();
        String issuanceBank = payload.getCardIssuanceBank();

        // Validate that the bank name is not empty/whitespace
        if (issuanceBank.isBlank()) {
            return ResponseEntity.badRequest().body(400); // 400 Bad Request
        }

        // Create CreditCard from payload and assign user
        CreditCard card = new CreditCard();
        card.setUser(user);
        card.setNumber(cardNumber);
        card.setIssuanceBank(issuanceBank);
        card.setBalanceHistories(new ArrayList<>());
        creditCardRepository.save(card);

        // Save card in the user object
        user.getCards().add(card);
        userRepository.save(user);

        return ResponseEntity.ok(card.getId());
    }

    @GetMapping("/credit-card:all")
    public ResponseEntity<List<CreditCardView>> getAllCardOfUser(@RequestParam int userId) {
        User user;
        List<CreditCardView> emptyList = new ArrayList<>();

        // Validate userID. Returns empty list if id is null, not found in repository, or if user obj failed to return.
        try {
            user = userRepository.findById(userId).orElseThrow();
        } catch (IllegalArgumentException | EmptyResultDataAccessException | NoSuchElementException ex) {
            return ResponseEntity.badRequest().body(emptyList); // User not found, return empty list
        }

        // Create creditCardView for each credit card and return list
        List<CreditCardView> cardViews = new ArrayList<>();
        for (CreditCard card: user.getCards()) {
            cardViews.add(new CreditCardView(card.getIssuanceBank(), card.getNumber()));
        }
        return ResponseEntity.ok(cardViews);
    }

    @GetMapping("/credit-card:user-id")
    public ResponseEntity<Integer> getUserIdForCreditCard(@RequestParam String creditCardNumber) {
        CreditCard card = creditCardRepository.findByNumber(creditCardNumber);
        if (card != null) {
            User user = card.getUser();
            if (user != null) {
                return ResponseEntity.ok(user.getId());
            }
        }
        return ResponseEntity.badRequest().body(400);
    }

    @GetMapping("/credit-card:history")
    public ResponseEntity<List<BalanceHistory>> getBalanceHistories(@RequestParam int cardId) {
        // Retrieves balance histories given a cardID.
        CreditCard card = creditCardRepository.findById(cardId).orElse(null);
        if (card != null) {
            return ResponseEntity.ok(card.getBalanceHistories());
        }
        return ResponseEntity.badRequest().body(null);
    }

    @PostMapping("/credit-card:update-balance")
    public ResponseEntity<Integer> updateBalanceHistory(@RequestBody UpdateBalancePayload[] payload) {
        List<CreditCard> cards = new ArrayList<>();

        for (UpdateBalancePayload transaction: payload) {
            String creditCardNumber = transaction.getCreditCardNumber();
            Instant transactionTime = transaction.getTransactionTime().truncatedTo(ChronoUnit.DAYS);
            double transactionAmount = transaction.getTransactionAmount();

            // Validate credit card number
            CreditCard card = creditCardRepository.findByNumber(creditCardNumber);
            if (card == null) {
                return ResponseEntity.badRequest().body(400);
            }

            List<BalanceHistory> history = card.getBalanceHistories();

            // Empty history, add transaction as first entry
            if (history.isEmpty()) {
                BalanceHistory newEntry = new BalanceHistory();
                newEntry.setDate(transactionTime);
                newEntry.setBalance(transactionAmount);
                history.add(newEntry);

                card.setBalanceHistories(history);
                cards.add(card);
                continue;
            }

            int closest = -1; // index of entry with the closest date to transaction
            double closestBalance = 0;
            boolean matched = false; // found matching BH in history

            // Find index of BH that matches transaction's date, or the closest
            for (int i = history.size() - 1; i >= 0; i -= 1) {
                BalanceHistory curr = history.get(i);
                Instant currentDay = curr.getDate().truncatedTo(ChronoUnit.DAYS);
                // If dates match
                if (currentDay.equals(transactionTime)) {
                    curr.setBalance(curr.getBalance() + transactionAmount);
                    matched = true;
                    break;
                }
                if (currentDay.isBefore(transactionTime)) {
                    closest = i;
                    closestBalance = curr.getBalance();
                }
            }

            // If matching BH entry not found, create new one and insert into the closest spot
            if (!matched) {
                BalanceHistory newEntry = new BalanceHistory();
                newEntry.setDate(transactionTime);
                newEntry.setBalance(closestBalance + transactionAmount);
                if (closest == -1) { // New transaction is before any other existing entry in time
                    history.add(newEntry);
                } else {
                    history.add(closest, newEntry);
                }
            }

            boolean startAdding = false; // Flag to start adding transactionAmount to balances
            boolean justAddedNewEntry = false; // Flag to not over add transactionAmount
            // Fill in gaps with new entries starting from the earliest entry
            for (int i = history.size() - 1; i >= 0; i -= 1) {
                BalanceHistory curr = history.get(i);
                Instant currentDay = curr.getDate().truncatedTo(ChronoUnit.DAYS);

                // If BH with new balance reached
                if (currentDay.equals(transactionTime)) {
                    startAdding = true;
                }

                BalanceHistory nextBH = (i > 0) ? history.get(i - 1) : null;
                Instant nextDay = currentDay.plus(1, ChronoUnit.DAYS);

                // No next BH, curr is the most recent entry
                if (nextBH == null) {
                    if (!justAddedNewEntry && startAdding && !currentDay.equals(transactionTime)) {
                        curr.setBalance(curr.getBalance() + transactionAmount);
                    }
                    justAddedNewEntry = false;
                }
                // next BH is the exact next day
                else if (nextBH.getDate().truncatedTo(ChronoUnit.DAYS).equals(nextDay)) {
                    if (!justAddedNewEntry && startAdding && !currentDay.equals(transactionTime)) {
                        curr.setBalance(curr.getBalance() + transactionAmount);
                    }
                    justAddedNewEntry = false;
                }
                // next BH is not the exact next day, but it is after the current day
                else if (nextBH.getDate().truncatedTo(ChronoUnit.DAYS).isAfter(currentDay)) {
                    // fill gap: create new BH and add at current index, set its balance to currentDay balance
                    BalanceHistory newEntry = new BalanceHistory();
                    newEntry.setDate(nextDay);
                    newEntry.setBalance(curr.getBalance());
                    history.add(i, newEntry);
                    justAddedNewEntry = true;
                    i += 1; // So curr points to the new entry after this loop
                }
            }
            card.setBalanceHistories(history);
            cards.add(card);
        }

        for (CreditCard card: cards) {
            creditCardRepository.save(card);
        }

        return ResponseEntity.ok(200);
    }
    
}
